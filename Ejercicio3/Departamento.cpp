#include <iostream>
#include "Departamento.h"

using namespace std;

/*Constructor*/
Departamento::Departamento(){
  int clave=0;
  int extension=0;
  string ubicacion="\0";
  int precio=0;
  bool disponibilidad=false;
}

/*Metodos get and set*/
int Departamento::get_clave(){
  return this->clave=clave;
}
int Departamento::get_extension(){
  return this->extension=extension;
}
string Departamento::get_ubicacion(){
  return this->ubicacion=ubicacion;
}
int Departamento::get_precio(){
  return this->precio=precio;
}
bool Departamento::get_disponibilidad(){
  return this->disponibilidad=disponibilidad;
}

void Departamento::set_clave(int clave){
  this->clave=clave;
}
void Departamento::set_extension(int extension){
  this->extension=extension;
}
void Departamento::set_ubicacion(string ubicacion){
  this->ubicacion=ubicacion;
}
void Departamento::set_precio(int precio){
  this->precio=precio;
}
void Departamento::set_disponibilidad(string disponibilidad){
  if (disponibilidad.compare("si") == 0){
		this->disponibilidad = true;
	}
	else{
	  this->disponibilidad = false;
	}
}
