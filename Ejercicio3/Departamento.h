#include <iostream>

using namespace std;

#ifndef DEPARTAMENTO_H
#define DEPARTAMENTO_H

class Departamento{
	private:
		int clave;
    int extension;
    string ubicacion;
    int precio;
    bool disponibilidad;
	public:
		/*Constructor*/
		Departamento();
		/*Metodos get and set*/
		int get_clave();
    int get_extension();
    string get_ubicacion();
    int get_precio();
    bool get_disponibilidad();

    void set_clave(int clave);
    void set_extension(int extension);
    void set_ubicacion(string ubicacion);
    void set_precio(int precio);
    void set_disponibilidad(string disponibilidad);

};
#endif
