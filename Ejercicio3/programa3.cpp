#include <iostream>
#include "Departamento.h"

using namespace std;

void tipoUbicacion(){
  cout << "- Excelente" << endl;
  cout << "- Buena" << endl;
  cout << "- Regular" << endl;
  cout << "- Mala" << endl;
}

void menu(){
  cout << "\n----------------------------------------------------Menu de Opciones----------------------------------------------------" << endl;
  cout << "(1) Listar datos de todos los departamentos disponibles con precio inferior o igual a cierto valor" << endl;
  cout << "(2) Listar datos de todos los departamentos disponibles con precio supero o igual a cierto valor y ubicacion excelente" << endl;
  cout << "(3) Listar el monto del arriendo de todos los departamentos arrendados" << endl;
  cout << "(4) Buscar Arriendo" << endl;
  cout << "(5) Aumentar el valor del arriendo un X%" << endl;
  cout << "(6) Salir" << endl;
  cout << "\nIngrese Opcion: ";
}

int main(){

  /*Variables*/
  string line;
  int N;
  int opcion=0;
  int precioBusqueda=0;
  int superficieBusqueda=0;
  string disponibilidadBusqueda="\0";
  int suma = 0;


  cout << "Cantidad de Departamentos a ingresar: ";
  getline(cin, line);
  N = stoi(line);

  Departamento *d = new Departamento[N];

  /*INgreso de Datos Departamentos*/
  cout << "---------------------------------------------------------" << endl;
  for (int i=0; i<N; i++){
    cout << "Datos Departamento " << i+1 << endl;
    cout << "Clave del Departamento: ";
    getline(cin, line);
    d[i].set_clave(stoi(line));

    cout << "Extension del Departamento(metros cuadrados): ";
    getline(cin, line);
    d[i].set_extension(stoi(line));

    cout << "Ubicacion del Departamento: " << endl;
    tipoUbicacion();
    getline(cin, line);
    d[i].set_ubicacion(line);

    cout << "Precio Arriendo del Departamento: ";
    getline(cin, line);
    d[i].set_precio(stoi(line));

    cout << "¿Se encuentra disponible el departamento?(si/no): ";
    getline(cin, line);
    d[i].set_disponibilidad(line);
    cout << "---------------------------------------------------------" << endl;
  }

  /*Menu*/
  while (opcion!=6){
    menu();
    getline(cin, line);
    opcion = stoi(line);
    while (opcion < 1 || opcion > 6){
      cout << "\n¡Opcion no valida!" << endl;
      menu();
      getline(cin, line);
      opcion = stoi(line);
    }

    switch (opcion){
      case 1:
          cout << "Ingrese Precio para buscar: ";
          getline(cin, line);
          precioBusqueda = stoi(line);
          cout << "\n-----------------------Departamentos Disponibles-----------------------" << endl;
          for (int i=0;i<N; i++){
            if (d[i].get_precio() <= precioBusqueda){
              if (d[i].get_precio()==true){
                cout << "Departamento " << i+1 << endl;
                cout << "Clave Departamento: " << d[i].get_clave() << endl;
                cout << "Extension Departamento: " << d[i].get_extension() << " metros cuadrados" << endl;
                cout << "Tipo de ubicacion: " << d[i].get_ubicacion() << endl;
                cout << "Precio Departamento: " << d[i].get_precio() << endl;
                cout << "-----------------------------------------------------------------------" << endl;
              }
            }
          }
          break;
      case 2:
          cout << "Buscar arriendo por superficie igual o mayor a: ";
          getline(cin, line);
          superficieBusqueda = stoi(line);

          for(int i=0; i<N; i++){
            cout << "Departamentos con excelente ubicacion" << endl;
            cout << "Superficies mayor o igual a " << superficieBusqueda << " metros cuadrados" << endl;
            if (d[i].get_disponibilidad() == true){
              if (d[i].get_ubicacion().compare("Excelente")){
                if(d[i].get_extension() >= superficieBusqueda){
                  cout << "----------------------------------------" << endl;
                  cout << "Departamento " << i+1 << endl;
                  cout << "Clave: " << d[i].get_clave() << endl;
                  cout << "Precio: " << d[i].get_precio() << endl;
                }
              }
            }
          }
          break;
      case 3:

          cout << "Monto de Departamentos Arrendados: " << endl;
          for (int i=0; i<N; i++){
            suma = d[i].get_precio() + suma;
          }
          cout << "Total: " << suma << " millones de pesos" << endl;
          break;
      case 4:
          cout << "Buscar Departamentos" << endl;
          cout << "Superficies mayor o igual a :";
          getline(cin, line);
          superficieBusqueda = stoi(line);

          cout << "Ingrese ubicacion deseada: ";
          tipoUbicacion();
          getline(cin, line);

          cout << "Ingrese precio departamento deseado en millones de pesos: ";
          getline(cin, line);
          precioBusqueda = stoi(line);

          for(int i=0; i<N; i++){
            if(d[i].get_extension() >= superficieBusqueda){

              if(d[i].get_ubicacion().compare(line) == 0){
                if(d[i].get_precio() == precioBusqueda){
                  d[i].set_disponibilidad("no");
                  cout << "--------------------------------------------------------------" << endl;
                  cout << "Departamento " << i+1 << " arrendado";
                  cout << "Clave: "<< d[i].get_clave() << endl;
                  cout << "Extension: "<< d[i].get_extension() << "metros cuadrado" << endl;
                  cout << "Ubicacion: "<< d[i].get_ubicacion() << endl;
                  cout << "Precio: " << d[i].get_precio() << " millones de pesos" << endl;

                }
              }
            }
          }
          break;
      case 5:
          int porcentaje = 0;
          cout << "\nIngrese valor x para realizar el aumento de precio: ";
          getline(cin, line);
          porcentaje = stoi(line);

          cout << "----------------------------------------" << endl;
          for (int i=0; i<N; i++){
            if (d[i].get_disponibilidad() == true){
              int precioNuevo = d[i].get_precio() + (d[i].get_precio()*porcentaje)/100;
              cout << "Precio Departamento: " << precioNuevo << endl;
            }
          }
          cout << "----------------------------------------" << endl;
          break;
    }
  }

  return 0;
}
