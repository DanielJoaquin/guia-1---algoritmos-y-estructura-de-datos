#include <iostream>
#include "Profesor.h"

using namespace std;

int main(){

	string line;
	int N=0;					//tamaño arreglo
	int e=0;					//edad
	int hombres=0;				//cantidad de hombres
	int mujeres=0;				//cantidad de mujeres
	int suma_promedio = 0;
	int promedio;
	int menor=500;				//variable para calcular edad menor
	int mayor=0;				//variable para calcular edad mayor
	int c1 = 0;					//variable para calcular cuantos profesores tienen edad menor al promedio
	int c2= 0;					//variable para calcular cuantos profesoras tienen edad mayor al promedio
	string nombre_menor;
	string nombre_mayor;
	cout << "Ingrese cantidad de profesores: ";
	getline(cin, line);
	N = stoi(line);

	Profesor *p = new Profesor[N];
	Profesor *p_hombres = new Profesor[N];
	Profesor *p_mujeres = new Profesor[N];

	cout << "\n---------------------------------------------------------" << endl;
	for(int i=0; i<N; i++){

		cout << "Ingrese sexo del profesor/a: 'h:hombre/m:mujer'";
		getline (cin, line);
		p[i].set_sexo(line);
		if (line == "h"){
			hombres = hombres +1;
			cout << "Ingrese el nombre del profesor/a: ";
			getline (cin, line);
			p[i].set_nombre(line);
			p_hombres[i].set_nombre(line);
			cout << "Ingrese la edad del profesor/a: ";
			getline (cin, line);
			e = stoi(line);
			p[i].set_edad(e);
			p_hombres[i].set_edad(e);
		}
		else if (line == "m"){
			mujeres = mujeres + 1;
			cout << "Ingrese el nombre del profesor/a: ";
			getline (cin, line);
			p[i].set_nombre(line);
			p_mujeres[i].set_nombre(line);
			cout << "Ingrese la edad del profesor/a: ";
			getline (cin, line);
			e = stoi(line);
			p[i].set_edad(e);
			p_mujeres[i].set_edad(e);
		}
		else{
			cout << "Ingrese un sexo correcto, porfavor" << endl;
			i--;
		}
		suma_promedio = suma_promedio + e;

	}
	cout << "---------------------------------------------------------" << endl;
	promedio = suma_promedio/N;
	cout << "\n---------------------------------------------------------" << endl;
	cout << "Edad Promedio de Profesores: " << promedio << endl;

	for(int i=0; i<N; i++){

		if(menor > p[i].get_edad()){
			menor = p[i].get_edad();
			nombre_menor = p[i].get_nombre();
		}
		if(mayor < p[i].get_edad()){
			mayor = p[i].get_edad();
			nombre_mayor = p[i].get_nombre();
		}
	}


	cout << "Nombre del Profesor/a con menor edad es : " << nombre_menor << endl;
	cout << "Nombre del Profesor/a con mayor edad es : " << nombre_mayor << endl;

	for(int i=0; i<hombres; i++){

		if(p_hombres[i].get_edad() < promedio){
			c1 = c1 + 1;
		}
	}
	for(int i=0; i<mujeres; i++){

		if(p_mujeres[i].get_edad() > promedio){
			c2 = c2 + 1;
		}
	}
	cout << "N° de profesoras con edad mayor al promedio : " << c2 << endl;
	cout << "N° de profesores con edad menor al promedio : " << c1 << endl;

	cout << "---------------------------------------------------------" << endl;
	return 0;
}
