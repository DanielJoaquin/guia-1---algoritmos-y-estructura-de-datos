#include <iostream>

using namespace std;

#ifndef PACIENTE_H
#define PACIENTE_H

class Paciente{
	private:
		string nombre;
		string sexo;
		string domicilio;
		int edad;
		int telefono;
		bool seguro;
	public:
		/*Constructor*/
		Paciente();
		/*Metodos get and set*/
		string get_Nombre();
		string get_Sexo();
		string get_Domicilio();
		int get_Edad();
		int get_Telefono();
		bool get_Seguro();

		void set_Nombre(string nombre);
		void set_Sexo(string sexo);
		void set_Domicilio(string domicilio);
		void set_Edad(int edad);
		void set_Telefono(int telefono);
		void set_Seguro(string seguro);

};
#endif
