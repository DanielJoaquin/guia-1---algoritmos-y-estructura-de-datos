#include <iostream>
#include "Paciente.h"

using namespace std;

Paciente::Paciente(){
	string nombre="\0";
	string sexo="\0";
	string domicilio="\0";
	int edad=0;
	int telefono=0;
	bool seguro=false;
}

/*Metodos get and set*/
string Paciente::get_Nombre(){
	return this->nombre;
}
string Paciente::get_Sexo(){
	return this->sexo;
}
string Paciente::get_Domicilio(){
	return this->domicilio;
}
int Paciente::get_Edad(){
	return this->edad;
}
int Paciente::get_Telefono(){
	return this->telefono;
}
bool Paciente::get_Seguro(){
	return this->seguro;
}

void Paciente::set_Nombre(string nombre){
	this->nombre = nombre;
}
void Paciente::set_Sexo(string sexo){
	this->sexo = sexo;
}
void Paciente::set_Domicilio(string domicilio){
	this->domicilio = domicilio;
}
void Paciente::set_Edad(int edad){
	this->edad = edad;
}
void Paciente::set_Telefono(int telefono){
	this->telefono = telefono;
}
void Paciente::set_Seguro(string seguro){
	if (seguro.compare("si") == 0){
		this->seguro = true;
	}
	else{
	  this->seguro = false;
	}

}
