#include <iostream>
#include "Paciente.h"
using namespace std;

void menu(){
  cout << "\n--------------------------Menu de Opciones--------------------------" << endl;
  cout << "(1) Imprimir lista de nombre de los Pacientes Hospitalizados" << endl;
  cout << "(2) Imprimir Porcentaje de los Pacientes Hospitalizados (por edad)" << endl;
  cout << "(3) Imprimir Porcentaje Hombres y Mujeres Hospitalizados" << endl;
  cout << "(4) Buscar datos de un Paciente por Nombre" << endl;
  cout << "(5) Calcular el Porcentaje de Pacietes con Seguro Medico" << endl;
  cout << "(6) Salir" << endl;
  cout << "\nIngrese Opcion: ";
}

int main(){

  /*Variables*/
  string line;
  int N;
  float ninos=0;
  float jovenes=0;
  float adultos=0;
  float hombres=0;
  float mujeres=0;

  cout << "Cantidad de Pacientes a ingresar: ";
  getline(cin, line);
  N = stoi(line);

  Paciente *p = new Paciente[N];
  /*INgreso de Datos Paciente*/
  for (int i=0; i<N; i++){

    cout << "-------------------------------------------" << endl;
    cout << "Datos Paciente " << i+1 << endl;
    cout << "Nombre Paciente: ";
    getline(cin, line);
    p[i].set_Nombre(line);

    cout << "Edad Paciente: ";
    getline(cin, line);
    p[i].set_Edad(stoi(line));
    if (stoi(line) < 13){
      ninos++;
    }
    else if(stoi(line) > 30){
      adultos++;
    }
    else{
      jovenes++;
    }

    cout << "Sexo Paciente (masculino o femenino): ";
    getline(cin, line);
    p[i].set_Sexo(line);
    if (line.compare("masculino")==0){
      hombres++;
    }
    if (line.compare("femenino")==0){
      mujeres++;
    }

    cout << "Domicilio Paciente: ";
    getline(cin, line);
    p[i].set_Domicilio(line);

    cout << "Telefono Paciente: ";
		getline(cin, line);
		p[i].set_Telefono(stoi(line));

		cout << "¿Posee seguro medico? (si/no): ";
		getline(cin, line);
		p[i].set_Seguro(line);
  }
  cout << "-------------------------------------------" << endl;

  /*Porcentajes*/
  ninos = (ninos/N)*100;
  jovenes = (jovenes/N)*100;
  adultos = (adultos/N)*100;
  hombres = (hombres/N)*100;
  mujeres = (mujeres/N)*100;

  /*Menu*/
  int opcion=0;
  int cont=0;
  while(opcion!=6){
    menu();
    getline(cin, line);
    opcion = stoi(line);
    while(opcion < 1 || opcion > 6){
      cout << "\n¡Opcion no valida!" << endl;
      menu();
      getline(cin, line);
      opcion = stoi(line);
    }

    switch (opcion) {
      case 1:
          cout << "--------------------------------------------" << endl;
          cout << "Nombre(s) de Paciente(s) Hospitalizado(s):" << endl;
          for (int i=0; i<N; i++){
            cout << "| " << p[i].get_Nombre() << " |" << endl;
          }
          cout << "--------------------------------------------" << endl;
          break;
      case 2:

          cout << "---------------------------------------" << endl;
          cout << "Porcentaje de Pacientes Hospitalizados" << endl;
          cout << "Niños: " << ninos << "%" << endl;
          cout << "Jovenes: " << jovenes << "%" << endl;
          cout << "Adultos: " << adultos << "%" << endl;
          cout << "---------------------------------------" << endl;
          break;
      case 3:
          cout << "---------------------------------------------------" << endl;
          cout << "Porcentaje de Hombre(s) y Mujer(es) Hospitalizados" << endl;
          cout << "Hombres: " << hombres << "%" << endl;
          cout << "Mujeres: " << mujeres << "%" << endl;
          cout << "---------------------------------------------------" << endl;
          break;
      case 4:
          cout << "\nIngrese nombre del Paciente a Buscar: ";
          getline(cin, line);
          for (int i=0; i<N; i++){
            if (p[i].get_Nombre().compare(line)==0){
              cout << "Nombre Paciente: " << p[i].get_Nombre() << endl;
              cout << "Edad: " << p[i].get_Edad() << endl;
              cout << "Sexo: " << p[i].get_Sexo() << endl;
              cout << "Domicilio: " << p[i].get_Domicilio() << endl;
              cout << "Telefono: " << p[i].get_Telefono() << endl;
              cout << "Seguro Medico: ";
              if (p[i].get_Seguro()==true){
                cout << "Si" << endl;
              }
              else{
                cout << "NO" << endl;
              }
              cont = cont + 1;
            }
          }
          if (cont == 0){
            cout << "No esta registrado un Paciente con ese Nombre" << endl;
          }
          break;
      case 5:
          float seguros =0;
          for (int i =0; i<N; i++){
            if(p[i].get_Seguro()==true){
              seguros = seguros + 1;
            }
          }
          seguros = (seguros/N)*100;
          cout << "Porcentaje de Pacientes con seguro medico: " << seguros << endl;
          break;
    }
  }

  return 0;
}
